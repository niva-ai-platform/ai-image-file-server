# syntax=docker/dockerfile:1

FROM node:18.5.0

ARG GITBRANCH="local"
ARG GITSHA="local"

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm ci

COPY . .
RUN npm run build

EXPOSE 3000

LABEL gitbranch=${GITBRANCH}
LABEL gitsha=${GITSHA}

ENV GITBRANCH=${GITBRANCH}
ENV GITSHA=${GITSHA}

CMD [ "npm", "start" ]
