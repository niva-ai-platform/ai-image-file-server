import { Request, Response } from 'express';
import multer from 'multer';
import fs from 'fs';
import  { v4 as uuid } from 'uuid';
import glob from 'glob';
import axios from 'axios';

const IMAGE_FS_HOST = process.env.IMAGE_FS_HOST || `https://niva-ai-dev.tssg.org`
export const BACKEND_HOST = process.env.BACKEND_HOST || `http://ai-be:3000`;
const appendStrings = ['fd', 'dlp'];
const uuidRegEx = /[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}/;
const appIdRegEx = /_([0-9][^a-z_]*)_/;
// const fdAppendedRegEx = /.+(?=-fd\.\w+$)/;

export const storage = multer.diskStorage({
  destination: async (req, file, cb) => {
    let path = 'files/';
    let applicationId;

    if (req.params.applicationid) {
      applicationId = `${req.params.applicationid}/`;
      // path = `files/${applicationId}`;
      // fs.mkdirSync(path, { recursive: true })
    } else {
      // if file has been previously processed, make sure the applicationId matches the applicationid from the filename
      if (fileIsAlreadyProcessed(file.originalname, uuidRegEx, regExAppendedString, fileNameIsAppended)){
        // retrieve app id from filename
        const appid = appIdRegEx.exec(file.originalname)[1];
        applicationId = `${appid}/`;
      } else {
        const token = req.headers.authorization;

        // Since appid is not provided, one is created from the backend and added to the req.params
        try {
          const response = await axios.get(`${BACKEND_HOST}/api/v1/application/start/niva`,
            {
              headers: {
                'Authorization': `${token}`
              }
            }
          );
          const appid = response.data.data.applicationid;
          applicationId = `${appid}/`;
          req.params.applicationid = appid;
          console.log(`Created new application: ${appid}`);
        } catch (err) {
          console.log(err);
        }
      }
    }
    path = `files/${applicationId}`;
    fs.mkdirSync(path, { recursive: true })
    return cb(null, path);
  },

  // By default, multer removes file extensions so let's add them back
  filename: (req, file, cb) => {
    const defaultFileName = `${Date.now()}_${req.params.applicationid ? req.params.applicationid + '_' : ''}${uuid()}_CERT_${file.originalname}`;

    if (fileIsAlreadyProcessed(file.originalname, uuidRegEx, regExAppendedString, fileNameIsAppended)) {
      glob('files/**/*.*', (err, files) => {
        if (err) {
          console.log(err);
        }
        let fileNameNoAppendixNoExtension: string;

        // check each possible appendix against filename
        for (let appendix of appendStrings) {
          // if an appendix is found on a filename set the filename and break the loop
          let regExResult = regExAppendedString(appendix).exec(file.originalname);
          if (regExResult) {
            fileNameNoAppendixNoExtension = regExResult[0];
            break;
          }
        }
        // const fileNameNoAppendixNoExtension = fdAppendedRegEx.exec(file.originalname)[0];
        const foundFilePath = files.filter(file => file.includes(fileNameNoAppendixNoExtension))[0];

        cb(null, foundFilePath.length > 0 ? file.originalname : defaultFileName);
      });

    } else {
      cb(null, defaultFileName);
    }
  }
});

export const processUpload = (req: Request, res: Response) => {
  const upload = multer({ storage: storage }).array('files');

  upload(req, res, (err) => {
      processResponse(req, res, err);
  });
};

export const processResponse = async (req: Request, res: Response, err: Error) => {
  if (!req.files || req.files.length === 0) {
      return res.send('Please select images to upload');
  }
  else if (err instanceof multer.MulterError) {
      return res.send(err);
  }
  else if (err) {
      return res.send(err);
  }

  const files: any = req.files;
  const uploadedFiles: any[] = [];

  for (const file of files) {
      uploadedFiles.push(
          `${IMAGE_FS_HOST}/image-fs/${file.path}`
      );

      if (!fileIsAlreadyProcessed(file.filename, uuidRegEx, regExAppendedString, fileNameIsAppended)) {
        try {
          const token = req.headers.authorization;
          const appid = req.params.applicationid;
          const response = await axios.post(`${BACKEND_HOST}/api/v1/evidence/upload/${appid}/${file.filename}`,
            {
              // No data
            },
            {
              headers: {
                'Authorization': `${token}`
              }
            }
          );
          console.log(`Record saved as id: ${response.data.insertId}`);
        } catch (err) {
          console.log(err);
        }
      }
  };

  return res.status(200).json({status: 200, uploadedFiles: uploadedFiles});
};

const fileIsAlreadyProcessed = (filename: string, uuidRegEx: RegExp, regExAppendedString: any, fileNameIsAppended: any) => {
  let isProcessedFile = false;
  isProcessedFile = uuidRegEx.test(filename);
  // isProcessedFile = fdAppendedRegEx.test(filename);
  // for (let string of appendStrings) {
  //   if (regExAppendedString(string).test(filename)) {
  //     isProcessedFile = true;
  //     break;
  //   } else {
  //     isProcessedFile = false;
  //   };
  // }
  isProcessedFile = fileNameIsAppended(filename, regExAppendedString, appendStrings);
  return isProcessedFile;
};

const fileNameIsAppended = (filename: string, regExAppendedString: any, appendStrings: string[]) => {
  let isAppended = false;
  for (let string of appendStrings) {
    if (regExAppendedString(string).test(filename)) {
      isAppended = true;
      break;
    } else {
      isAppended = false;
    };
  }
  return isAppended;
};

export const regExAppendedString = (appendedString: string) => {
  const regEx = new RegExp(`.+(?=-${appendedString}\\.\\w+$)`);
  return regEx;
};
