import express, { Application, Request, Response } from 'express';
import session from 'express-session';
import 'dotenv/config';
import Keycloak from 'keycloak-connect';
import axios from 'axios';

import { BACKEND_HOST, regExAppendedString } from './utils/process-image';
import { processUpload } from './utils/process-image';

let healthy = false;
const PORT = process.env.PORT || 3000;
const app: Application = express();
const router = express.Router();

// Create a session-store to be used by both the express-session
// middleware and the keycloak middleware.
const memoryStore = new session.MemoryStore();

app.use(session({
  secret: 'someSecret',
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));

// Provide the session store to the Keycloak so that sessions
// can be invalidated from the Keycloak console callback.
//
// Additional configuration is read from keycloak.json file
// installed from the Keycloak web console.
const keycloak = new Keycloak({
  store: memoryStore,
});

router.use(keycloak.middleware({
  logout: '/logout',
  admin: '/'
}));

app.use(express.static('/files'));

router.post('/file_upload',
  // Call to tag service ?

  // realm:upload-image is a reference to the realm role 'upload-image' which is mapped to the logged in user
  keycloak.protect('realm:upload-image'),
  async (req: Request, res: Response) => {
    // const token = req.headers.authorization;

    // // Since appid is not provided, one is created from the backend and added to the req.params
    // try {
    //   const response = await axios.get(`${BACKEND_HOST}/api/v1/application/start/niva`,
    //     {
    //       headers: {
    //         'Authorization': `${token}`
    //       }
    //     }
    //   );
    //   const appid = response.data.data.applicationid;
    //   req.params.batch = appid;
    //   console.log(`Created new application: ${appid}`);
    // } catch (err) {
    //   console.log(err);
    // }
    processUpload(req, res);
  }
);

router.post('/file_upload/:applicationid',
  keycloak.protect('realm:upload-image'),
  async (req: Request, res: Response) => {
    const token = req.headers.authorization;
    let appid = req.params.applicationid;
    // Check that appid is valid
    try {
      const response = await axios.get(`${BACKEND_HOST}/api/v1/application/get/${appid}`,
        {
          headers: {
            'Authorization': `${token}`
          }
        }
      );
      if (!response.data.success) {
        res.json({
          success: false,
          message: 'Invalid Application ID'
        })
      } else {
        processUpload(req, res);
      }
    } catch (err) {
      console.log(err);
    }
  }
);

router.get('/files/:appid/:filename',
  keycloak.protect('realm:upload-image'),
  (req: Request, res: Response) => {
    const appid = req.params.appid;
    const filename = req.params.filename;

    res.download(`files/${appid}/${filename}`, (err) => {
        if (err) {
            console.log('ERROR', err);
        }
    });
});

router.get('/health', (req: Request, res: Response) => {
  res.status(200).json({
    healthy: healthy,
    gitBranch: process.env.GITBRANCH,
    gitSha: process.env.GITSHA
  });
});

router.get('/stop', (req: Request, res: Response) => {
  // TODO
  res.status(200).json({
    status: 200,
    shutDown: false
  });
});

app.use('/image-fs',
(req, res, next) => {
  const token = req.query.token;
  // If request has a token query, add the token as an Authorization header
  if (token) req.headers.authorization = `Bearer ${req.query.token}`
  next();
},
router);

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));

healthy = true;
